<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.4.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="16" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="diode">
<description>&lt;b&gt;Diodes&lt;/b&gt;&lt;p&gt;
Based on the following sources:
&lt;ul&gt;
&lt;li&gt;Motorola : www.onsemi.com
&lt;li&gt;Fairchild : www.fairchildsemi.com
&lt;li&gt;Philips : www.semiconductors.com
&lt;li&gt;Vishay : www.vishay.de
&lt;/ul&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DO41-10">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
diameter 2.54 mm, horizontal, grid 10.16 mm</description>
<wire x1="2.032" y1="-1.27" x2="-2.032" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-1.27" x2="2.032" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="1.27" x2="2.032" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="1.27" x2="-2.032" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.762" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.762" layer="51"/>
<wire x1="-0.635" y1="0" x2="0" y2="0" width="0.1524" layer="21"/>
<wire x1="1.016" y1="0.635" x2="1.016" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-0.635" x2="0" y2="0" width="0.1524" layer="21"/>
<wire x1="0" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0" y1="0" x2="1.016" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="0" width="0.1524" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="A" x="5.08" y="0" drill="1.1176"/>
<pad name="C" x="-5.08" y="0" drill="1.1176"/>
<text x="-2.032" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.27" x2="-1.143" y2="1.27" layer="21"/>
<rectangle x1="2.032" y1="-0.381" x2="3.937" y2="0.381" layer="21"/>
<rectangle x1="-3.937" y1="-0.381" x2="-2.032" y2="0.381" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="D">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="-2.54" y="0" size="0.4064" layer="99" align="center">SpiceOrder 1</text>
<text x="2.54" y="0" size="0.4064" layer="99" align="center">SpiceOrder 2</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="1N4004" prefix="D">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
general purpose rectifier, 1 A</description>
<gates>
<gate name="1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO41-10">
<connects>
<connect gate="1" pin="A" pad="A"/>
<connect gate="1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-wago-500">
<description>&lt;b&gt;Wago Screw Clamps&lt;/b&gt;&lt;p&gt;
Grid 5.00 mm&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="W237-102">
<description>&lt;b&gt;WAGO SCREW CLAMP&lt;/b&gt;</description>
<wire x1="-3.491" y1="-2.286" x2="-1.484" y2="-0.279" width="0.254" layer="51"/>
<wire x1="1.488" y1="-2.261" x2="3.469" y2="-0.254" width="0.254" layer="51"/>
<wire x1="-4.989" y1="-5.461" x2="4.993" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.734" x2="4.993" y2="3.531" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.734" x2="-4.989" y2="3.734" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-5.461" x2="-4.989" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-3.073" x2="-3.389" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-3.389" y1="-3.073" x2="-1.611" y2="-3.073" width="0.1524" layer="51"/>
<wire x1="-1.611" y1="-3.073" x2="1.615" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="3.393" y1="-3.073" x2="4.993" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="-3.073" x2="-4.989" y2="3.531" width="0.1524" layer="21"/>
<wire x1="4.993" y1="-3.073" x2="4.993" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="3.531" x2="4.993" y2="3.531" width="0.1524" layer="21"/>
<wire x1="-4.989" y1="3.531" x2="-4.989" y2="3.734" width="0.1524" layer="21"/>
<wire x1="4.993" y1="3.531" x2="4.993" y2="-3.073" width="0.1524" layer="21"/>
<wire x1="1.615" y1="-3.073" x2="3.393" y2="-3.073" width="0.1524" layer="51"/>
<circle x="-2.5" y="-1.27" radius="1.4986" width="0.1524" layer="51"/>
<circle x="-2.5" y="2.2098" radius="0.508" width="0.1524" layer="21"/>
<circle x="2.5038" y="-1.27" radius="1.4986" width="0.1524" layer="51"/>
<circle x="2.5038" y="2.2098" radius="0.508" width="0.1524" layer="21"/>
<pad name="1" x="-2.5" y="-1.27" drill="1.1938" shape="long" rot="R90"/>
<pad name="2" x="2.5" y="-1.27" drill="1.1938" shape="long" rot="R90"/>
<text x="-5.04" y="-7.62" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.8462" y="-5.0038" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.532" y="0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="0.421" y="0.635" size="1.27" layer="21" ratio="10">2</text>
</package>
</packages>
<symbols>
<symbol name="KL">
<circle x="1.27" y="0" radius="1.27" width="0.254" layer="94"/>
<text x="0" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="KL+V">
<circle x="1.27" y="0" radius="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="-3.683" size="1.778" layer="96">&gt;VALUE</text>
<text x="0" y="0.889" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<pin name="KL" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="W237-102" prefix="X" uservalue="yes">
<description>&lt;b&gt;WAGO SCREW CLAMP&lt;/b&gt;</description>
<gates>
<gate name="-1" symbol="KL" x="0" y="5.08" addlevel="always"/>
<gate name="-2" symbol="KL+V" x="0" y="0" addlevel="always"/>
</gates>
<devices>
<device name="" package="W237-102">
<connects>
<connect gate="-1" pin="KL" pad="1"/>
<connect gate="-2" pin="KL" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="237-102" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="70K9898" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="D1" library="diode" deviceset="1N4004" device=""/>
<part name="D2" library="diode" deviceset="1N4004" device=""/>
<part name="D3" library="diode" deviceset="1N4004" device=""/>
<part name="D4" library="diode" deviceset="1N4004" device=""/>
<part name="D5" library="diode" deviceset="1N4004" device=""/>
<part name="D6" library="diode" deviceset="1N4004" device=""/>
<part name="D7" library="diode" deviceset="1N4004" device=""/>
<part name="D8" library="diode" deviceset="1N4004" device=""/>
<part name="D9" library="diode" deviceset="1N4004" device=""/>
<part name="D10" library="diode" deviceset="1N4004" device=""/>
<part name="D11" library="diode" deviceset="1N4004" device=""/>
<part name="D12" library="diode" deviceset="1N4004" device=""/>
<part name="D13" library="diode" deviceset="1N4004" device=""/>
<part name="D14" library="diode" deviceset="1N4004" device=""/>
<part name="D15" library="diode" deviceset="1N4004" device=""/>
<part name="D16" library="diode" deviceset="1N4004" device=""/>
<part name="D17" library="diode" deviceset="1N4004" device=""/>
<part name="D18" library="diode" deviceset="1N4004" device=""/>
<part name="D19" library="diode" deviceset="1N4004" device=""/>
<part name="D20" library="diode" deviceset="1N4004" device=""/>
<part name="D21" library="diode" deviceset="1N4004" device=""/>
<part name="D22" library="diode" deviceset="1N4004" device=""/>
<part name="D23" library="diode" deviceset="1N4004" device=""/>
<part name="D24" library="diode" deviceset="1N4004" device=""/>
<part name="D25" library="diode" deviceset="1N4004" device=""/>
<part name="D26" library="diode" deviceset="1N4004" device=""/>
<part name="D27" library="diode" deviceset="1N4004" device=""/>
<part name="D28" library="diode" deviceset="1N4004" device=""/>
<part name="D29" library="diode" deviceset="1N4004" device=""/>
<part name="D30" library="diode" deviceset="1N4004" device=""/>
<part name="D32" library="diode" deviceset="1N4004" device=""/>
<part name="D31" library="diode" deviceset="1N4004" device=""/>
<part name="D33" library="diode" deviceset="1N4004" device=""/>
<part name="D34" library="diode" deviceset="1N4004" device=""/>
<part name="D35" library="diode" deviceset="1N4004" device=""/>
<part name="D36" library="diode" deviceset="1N4004" device=""/>
<part name="X1" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X2" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X3" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X4" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X5" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X6" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X7" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X8" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X9" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X10" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X11" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X12" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X13" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X14" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X15" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X16" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X17" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X18" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X19" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X20" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X21" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X22" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X23" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X24" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X25" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X26" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X27" library="con-wago-500" deviceset="W237-102" device=""/>
<part name="X28" library="con-wago-500" deviceset="W237-102" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="D1" gate="1" x="68.58" y="88.9"/>
<instance part="D2" gate="1" x="83.82" y="88.9"/>
<instance part="D3" gate="1" x="99.06" y="96.52"/>
<instance part="D4" gate="1" x="68.58" y="63.5"/>
<instance part="D5" gate="1" x="83.82" y="63.5"/>
<instance part="D6" gate="1" x="99.06" y="71.12"/>
<instance part="D7" gate="1" x="68.58" y="38.1"/>
<instance part="D8" gate="1" x="83.82" y="38.1"/>
<instance part="D9" gate="1" x="99.06" y="45.72"/>
<instance part="D10" gate="1" x="68.58" y="12.7"/>
<instance part="D11" gate="1" x="83.82" y="12.7"/>
<instance part="D12" gate="1" x="99.06" y="20.32"/>
<instance part="D13" gate="1" x="68.58" y="-12.7"/>
<instance part="D14" gate="1" x="83.82" y="-12.7"/>
<instance part="D15" gate="1" x="99.06" y="-5.08"/>
<instance part="D16" gate="1" x="68.58" y="-38.1"/>
<instance part="D17" gate="1" x="83.82" y="-38.1"/>
<instance part="D18" gate="1" x="99.06" y="-30.48"/>
<instance part="D19" gate="1" x="68.58" y="-63.5"/>
<instance part="D20" gate="1" x="83.82" y="-63.5"/>
<instance part="D21" gate="1" x="99.06" y="-55.88"/>
<instance part="D22" gate="1" x="68.58" y="-88.9"/>
<instance part="D23" gate="1" x="83.82" y="-88.9"/>
<instance part="D24" gate="1" x="99.06" y="-81.28"/>
<instance part="D25" gate="1" x="68.58" y="-114.3"/>
<instance part="D26" gate="1" x="83.82" y="-114.3"/>
<instance part="D27" gate="1" x="99.06" y="-106.68"/>
<instance part="D28" gate="1" x="68.58" y="-139.7"/>
<instance part="D29" gate="1" x="83.82" y="-139.7"/>
<instance part="D30" gate="1" x="99.06" y="-132.08"/>
<instance part="D32" gate="1" x="154.94" y="-81.28"/>
<instance part="D31" gate="1" x="154.94" y="-93.98"/>
<instance part="D33" gate="1" x="154.94" y="-106.68"/>
<instance part="D34" gate="1" x="154.94" y="-119.38"/>
<instance part="D35" gate="1" x="154.94" y="-132.08"/>
<instance part="D36" gate="1" x="154.94" y="-144.78"/>
<instance part="X1" gate="-1" x="81.28" y="101.6" rot="R270"/>
<instance part="X1" gate="-2" x="86.36" y="101.6" rot="R270"/>
<instance part="X2" gate="-1" x="66.04" y="101.6" rot="R270"/>
<instance part="X2" gate="-2" x="71.12" y="101.6" rot="R270"/>
<instance part="X3" gate="-1" x="81.28" y="76.2" rot="R270"/>
<instance part="X3" gate="-2" x="86.36" y="76.2" rot="R270"/>
<instance part="X4" gate="-1" x="66.04" y="76.2" rot="R270"/>
<instance part="X4" gate="-2" x="71.12" y="76.2" rot="R270"/>
<instance part="X5" gate="-1" x="81.28" y="50.8" rot="R270"/>
<instance part="X5" gate="-2" x="86.36" y="50.8" rot="R270"/>
<instance part="X6" gate="-1" x="66.04" y="50.8" rot="R270"/>
<instance part="X6" gate="-2" x="71.12" y="50.8" rot="R270"/>
<instance part="X7" gate="-1" x="81.28" y="25.4" rot="R270"/>
<instance part="X7" gate="-2" x="86.36" y="25.4" rot="R270"/>
<instance part="X8" gate="-1" x="66.04" y="25.4" rot="R270"/>
<instance part="X8" gate="-2" x="71.12" y="25.4" rot="R270"/>
<instance part="X9" gate="-1" x="81.28" y="0" rot="R270"/>
<instance part="X9" gate="-2" x="86.36" y="0" rot="R270"/>
<instance part="X10" gate="-1" x="66.04" y="0" rot="R270"/>
<instance part="X10" gate="-2" x="71.12" y="0" rot="R270"/>
<instance part="X11" gate="-1" x="81.28" y="-50.8" rot="R270"/>
<instance part="X11" gate="-2" x="86.36" y="-50.8" rot="R270"/>
<instance part="X12" gate="-1" x="66.04" y="-50.8" rot="R270"/>
<instance part="X12" gate="-2" x="71.12" y="-50.8" rot="R270"/>
<instance part="X13" gate="-1" x="81.28" y="-76.2" rot="R270"/>
<instance part="X13" gate="-2" x="86.36" y="-76.2" rot="R270"/>
<instance part="X14" gate="-1" x="66.04" y="-76.2" rot="R270"/>
<instance part="X14" gate="-2" x="71.12" y="-76.2" rot="R270"/>
<instance part="X15" gate="-1" x="81.28" y="-101.6" rot="R270"/>
<instance part="X15" gate="-2" x="86.36" y="-101.6" rot="R270"/>
<instance part="X16" gate="-1" x="66.04" y="-101.6" rot="R270"/>
<instance part="X16" gate="-2" x="71.12" y="-101.6" rot="R270"/>
<instance part="X17" gate="-1" x="81.28" y="-127" rot="R270"/>
<instance part="X17" gate="-2" x="86.36" y="-127" rot="R270"/>
<instance part="X18" gate="-1" x="66.04" y="-127" rot="R270"/>
<instance part="X18" gate="-2" x="71.12" y="-127" rot="R270"/>
<instance part="X19" gate="-1" x="81.28" y="-167.64" rot="R90"/>
<instance part="X19" gate="-2" x="78.74" y="-167.64" rot="R90"/>
<instance part="X20" gate="-1" x="144.78" y="-76.2" rot="R270"/>
<instance part="X20" gate="-2" x="142.24" y="-76.2" rot="R270"/>
<instance part="X21" gate="-1" x="144.78" y="-88.9" rot="R270"/>
<instance part="X21" gate="-2" x="142.24" y="-88.9" rot="R270"/>
<instance part="X22" gate="-1" x="144.78" y="-101.6" rot="R270"/>
<instance part="X22" gate="-2" x="142.24" y="-101.6" rot="R270"/>
<instance part="X23" gate="-1" x="144.78" y="-114.3" rot="R270"/>
<instance part="X23" gate="-2" x="142.24" y="-114.3" rot="R270"/>
<instance part="X24" gate="-1" x="144.78" y="-127" rot="R270"/>
<instance part="X24" gate="-2" x="142.24" y="-127" rot="R270"/>
<instance part="X25" gate="-1" x="144.78" y="-139.7" rot="R270"/>
<instance part="X25" gate="-2" x="142.24" y="-139.7" rot="R270"/>
<instance part="X26" gate="-1" x="144.78" y="-160.02" rot="R270"/>
<instance part="X26" gate="-2" x="142.24" y="-160.02" rot="R270"/>
<instance part="X27" gate="-1" x="81.28" y="-25.4" rot="R270"/>
<instance part="X27" gate="-2" x="86.36" y="-25.4" rot="R270"/>
<instance part="X28" gate="-1" x="66.04" y="-25.4" rot="R270"/>
<instance part="X28" gate="-2" x="71.12" y="-25.4" rot="R270"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$2" class="0">
<segment>
<wire x1="71.12" y1="96.52" x2="76.2" y2="96.52" width="0.1524" layer="91"/>
<wire x1="76.2" y1="96.52" x2="81.28" y2="96.52" width="0.1524" layer="91"/>
<wire x1="76.2" y1="96.52" x2="76.2" y2="88.9" width="0.1524" layer="91"/>
<junction x="76.2" y="96.52"/>
<pinref part="D1" gate="1" pin="C"/>
<wire x1="76.2" y1="88.9" x2="71.12" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D2" gate="1" pin="A"/>
<wire x1="76.2" y1="88.9" x2="81.28" y2="88.9" width="0.1524" layer="91"/>
<junction x="76.2" y="88.9"/>
<pinref part="X2" gate="-2" pin="KL"/>
<pinref part="X1" gate="-1" pin="KL"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="86.36" y1="96.52" x2="91.44" y2="96.52" width="0.1524" layer="91"/>
<wire x1="91.44" y1="96.52" x2="91.44" y2="88.9" width="0.1524" layer="91"/>
<junction x="91.44" y="96.52"/>
<pinref part="D2" gate="1" pin="C"/>
<wire x1="91.44" y1="88.9" x2="86.36" y2="88.9" width="0.1524" layer="91"/>
<pinref part="D3" gate="1" pin="A"/>
<wire x1="91.44" y1="96.52" x2="96.52" y2="96.52" width="0.1524" layer="91"/>
<pinref part="X1" gate="-2" pin="KL"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="D4" gate="1" pin="C"/>
<pinref part="D5" gate="1" pin="A"/>
<wire x1="71.12" y1="63.5" x2="76.2" y2="63.5" width="0.1524" layer="91"/>
<wire x1="76.2" y1="63.5" x2="81.28" y2="63.5" width="0.1524" layer="91"/>
<wire x1="71.12" y1="71.12" x2="76.2" y2="71.12" width="0.1524" layer="91"/>
<wire x1="76.2" y1="71.12" x2="81.28" y2="71.12" width="0.1524" layer="91"/>
<wire x1="76.2" y1="71.12" x2="76.2" y2="63.5" width="0.1524" layer="91"/>
<junction x="76.2" y="71.12"/>
<pinref part="X4" gate="-2" pin="KL"/>
<pinref part="X3" gate="-1" pin="KL"/>
<junction x="76.2" y="63.5"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="91.44" y1="71.12" x2="91.44" y2="63.5" width="0.1524" layer="91"/>
<pinref part="D5" gate="1" pin="C"/>
<wire x1="91.44" y1="63.5" x2="86.36" y2="63.5" width="0.1524" layer="91"/>
<pinref part="D6" gate="1" pin="A"/>
<wire x1="91.44" y1="71.12" x2="96.52" y2="71.12" width="0.1524" layer="91"/>
<wire x1="86.36" y1="71.12" x2="91.44" y2="71.12" width="0.1524" layer="91"/>
<pinref part="X3" gate="-2" pin="KL"/>
<junction x="91.44" y="71.12"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="D7" gate="1" pin="C"/>
<pinref part="D8" gate="1" pin="A"/>
<wire x1="71.12" y1="38.1" x2="76.2" y2="38.1" width="0.1524" layer="91"/>
<wire x1="76.2" y1="38.1" x2="81.28" y2="38.1" width="0.1524" layer="91"/>
<wire x1="71.12" y1="45.72" x2="76.2" y2="45.72" width="0.1524" layer="91"/>
<wire x1="76.2" y1="45.72" x2="81.28" y2="45.72" width="0.1524" layer="91"/>
<wire x1="76.2" y1="45.72" x2="76.2" y2="38.1" width="0.1524" layer="91"/>
<junction x="76.2" y="45.72"/>
<pinref part="X6" gate="-2" pin="KL"/>
<pinref part="X5" gate="-1" pin="KL"/>
<junction x="76.2" y="38.1"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="91.44" y1="45.72" x2="91.44" y2="38.1" width="0.1524" layer="91"/>
<pinref part="D8" gate="1" pin="C"/>
<wire x1="91.44" y1="38.1" x2="86.36" y2="38.1" width="0.1524" layer="91"/>
<pinref part="D9" gate="1" pin="A"/>
<wire x1="91.44" y1="45.72" x2="96.52" y2="45.72" width="0.1524" layer="91"/>
<wire x1="86.36" y1="45.72" x2="91.44" y2="45.72" width="0.1524" layer="91"/>
<pinref part="X5" gate="-2" pin="KL"/>
<junction x="91.44" y="45.72"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="D10" gate="1" pin="C"/>
<pinref part="D11" gate="1" pin="A"/>
<wire x1="71.12" y1="12.7" x2="76.2" y2="12.7" width="0.1524" layer="91"/>
<wire x1="76.2" y1="12.7" x2="81.28" y2="12.7" width="0.1524" layer="91"/>
<wire x1="71.12" y1="20.32" x2="76.2" y2="20.32" width="0.1524" layer="91"/>
<wire x1="76.2" y1="20.32" x2="81.28" y2="20.32" width="0.1524" layer="91"/>
<wire x1="76.2" y1="20.32" x2="76.2" y2="12.7" width="0.1524" layer="91"/>
<junction x="76.2" y="20.32"/>
<pinref part="X8" gate="-2" pin="KL"/>
<pinref part="X7" gate="-1" pin="KL"/>
<junction x="76.2" y="12.7"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="91.44" y1="20.32" x2="91.44" y2="12.7" width="0.1524" layer="91"/>
<pinref part="D11" gate="1" pin="C"/>
<wire x1="91.44" y1="12.7" x2="86.36" y2="12.7" width="0.1524" layer="91"/>
<pinref part="D12" gate="1" pin="A"/>
<wire x1="91.44" y1="20.32" x2="96.52" y2="20.32" width="0.1524" layer="91"/>
<wire x1="86.36" y1="20.32" x2="91.44" y2="20.32" width="0.1524" layer="91"/>
<pinref part="X7" gate="-2" pin="KL"/>
<junction x="91.44" y="20.32"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="D13" gate="1" pin="C"/>
<pinref part="D14" gate="1" pin="A"/>
<wire x1="71.12" y1="-12.7" x2="76.2" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-12.7" x2="81.28" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-5.08" x2="76.2" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-5.08" x2="81.28" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-5.08" x2="76.2" y2="-12.7" width="0.1524" layer="91"/>
<junction x="76.2" y="-5.08"/>
<pinref part="X10" gate="-2" pin="KL"/>
<pinref part="X9" gate="-1" pin="KL"/>
<junction x="76.2" y="-12.7"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<wire x1="91.44" y1="-5.08" x2="91.44" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="D14" gate="1" pin="C"/>
<wire x1="91.44" y1="-12.7" x2="86.36" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="D15" gate="1" pin="A"/>
<wire x1="91.44" y1="-5.08" x2="96.52" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-5.08" x2="91.44" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="X9" gate="-2" pin="KL"/>
<junction x="91.44" y="-5.08"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<wire x1="91.44" y1="-30.48" x2="91.44" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="D17" gate="1" pin="C"/>
<wire x1="91.44" y1="-38.1" x2="86.36" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="D18" gate="1" pin="A"/>
<wire x1="91.44" y1="-30.48" x2="96.52" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-30.48" x2="91.44" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="X27" gate="-2" pin="KL"/>
<junction x="91.44" y="-30.48"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="D19" gate="1" pin="C"/>
<pinref part="D20" gate="1" pin="A"/>
<wire x1="71.12" y1="-63.5" x2="76.2" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-63.5" x2="81.28" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-55.88" x2="76.2" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-55.88" x2="81.28" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-55.88" x2="76.2" y2="-63.5" width="0.1524" layer="91"/>
<junction x="76.2" y="-55.88"/>
<pinref part="X12" gate="-2" pin="KL"/>
<pinref part="X11" gate="-1" pin="KL"/>
<junction x="76.2" y="-63.5"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<wire x1="91.44" y1="-55.88" x2="91.44" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="D20" gate="1" pin="C"/>
<wire x1="91.44" y1="-63.5" x2="86.36" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="D21" gate="1" pin="A"/>
<wire x1="91.44" y1="-55.88" x2="96.52" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-55.88" x2="91.44" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="X11" gate="-2" pin="KL"/>
<junction x="91.44" y="-55.88"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="D22" gate="1" pin="C"/>
<pinref part="D23" gate="1" pin="A"/>
<wire x1="71.12" y1="-88.9" x2="76.2" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-88.9" x2="81.28" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-81.28" x2="76.2" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-81.28" x2="81.28" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-81.28" x2="76.2" y2="-88.9" width="0.1524" layer="91"/>
<junction x="76.2" y="-81.28"/>
<pinref part="X14" gate="-2" pin="KL"/>
<pinref part="X13" gate="-1" pin="KL"/>
<junction x="76.2" y="-88.9"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<wire x1="91.44" y1="-81.28" x2="91.44" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="D23" gate="1" pin="C"/>
<wire x1="91.44" y1="-88.9" x2="86.36" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="D24" gate="1" pin="A"/>
<wire x1="91.44" y1="-81.28" x2="96.52" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-81.28" x2="91.44" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="X13" gate="-2" pin="KL"/>
<junction x="91.44" y="-81.28"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="D25" gate="1" pin="C"/>
<pinref part="D26" gate="1" pin="A"/>
<wire x1="71.12" y1="-114.3" x2="76.2" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-114.3" x2="81.28" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-106.68" x2="76.2" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-106.68" x2="81.28" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-106.68" x2="76.2" y2="-114.3" width="0.1524" layer="91"/>
<junction x="76.2" y="-106.68"/>
<pinref part="X16" gate="-2" pin="KL"/>
<pinref part="X15" gate="-1" pin="KL"/>
<junction x="76.2" y="-114.3"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<wire x1="91.44" y1="-106.68" x2="91.44" y2="-114.3" width="0.1524" layer="91"/>
<pinref part="D26" gate="1" pin="C"/>
<wire x1="91.44" y1="-114.3" x2="86.36" y2="-114.3" width="0.1524" layer="91"/>
<pinref part="D27" gate="1" pin="A"/>
<wire x1="91.44" y1="-106.68" x2="96.52" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-106.68" x2="91.44" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="X15" gate="-2" pin="KL"/>
<junction x="91.44" y="-106.68"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="D28" gate="1" pin="C"/>
<pinref part="D29" gate="1" pin="A"/>
<wire x1="71.12" y1="-139.7" x2="76.2" y2="-139.7" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-139.7" x2="81.28" y2="-139.7" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-132.08" x2="76.2" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-132.08" x2="81.28" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-132.08" x2="76.2" y2="-139.7" width="0.1524" layer="91"/>
<junction x="76.2" y="-132.08"/>
<pinref part="X18" gate="-2" pin="KL"/>
<pinref part="X17" gate="-1" pin="KL"/>
<junction x="76.2" y="-139.7"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<wire x1="91.44" y1="-132.08" x2="91.44" y2="-139.7" width="0.1524" layer="91"/>
<pinref part="D29" gate="1" pin="C"/>
<wire x1="91.44" y1="-139.7" x2="86.36" y2="-139.7" width="0.1524" layer="91"/>
<pinref part="D30" gate="1" pin="A"/>
<wire x1="91.44" y1="-132.08" x2="96.52" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-132.08" x2="91.44" y2="-132.08" width="0.1524" layer="91"/>
<pinref part="X17" gate="-2" pin="KL"/>
<junction x="91.44" y="-132.08"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="78.74" y1="-162.56" x2="55.88" y2="-162.56" width="0.1524" layer="91"/>
<wire x1="66.04" y1="96.52" x2="60.96" y2="96.52" width="0.1524" layer="91"/>
<wire x1="55.88" y1="96.52" x2="60.96" y2="96.52" width="0.1524" layer="91"/>
<wire x1="60.96" y1="96.52" x2="60.96" y2="88.9" width="0.1524" layer="91"/>
<junction x="60.96" y="96.52"/>
<pinref part="D1" gate="1" pin="A"/>
<wire x1="60.96" y1="88.9" x2="66.04" y2="88.9" width="0.1524" layer="91"/>
<wire x1="55.88" y1="71.12" x2="60.96" y2="71.12" width="0.1524" layer="91"/>
<wire x1="60.96" y1="71.12" x2="60.96" y2="63.5" width="0.1524" layer="91"/>
<pinref part="D4" gate="1" pin="A"/>
<wire x1="60.96" y1="63.5" x2="66.04" y2="63.5" width="0.1524" layer="91"/>
<wire x1="55.88" y1="45.72" x2="60.96" y2="45.72" width="0.1524" layer="91"/>
<wire x1="60.96" y1="45.72" x2="60.96" y2="38.1" width="0.1524" layer="91"/>
<pinref part="D7" gate="1" pin="A"/>
<wire x1="60.96" y1="38.1" x2="66.04" y2="38.1" width="0.1524" layer="91"/>
<wire x1="55.88" y1="20.32" x2="60.96" y2="20.32" width="0.1524" layer="91"/>
<wire x1="60.96" y1="20.32" x2="60.96" y2="12.7" width="0.1524" layer="91"/>
<pinref part="D10" gate="1" pin="A"/>
<wire x1="60.96" y1="12.7" x2="66.04" y2="12.7" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-5.08" x2="60.96" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-5.08" x2="60.96" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="D13" gate="1" pin="A"/>
<wire x1="60.96" y1="-12.7" x2="66.04" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-30.48" x2="60.96" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-30.48" x2="60.96" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="D16" gate="1" pin="A"/>
<wire x1="60.96" y1="-38.1" x2="66.04" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-55.88" x2="60.96" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-55.88" x2="60.96" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="D19" gate="1" pin="A"/>
<wire x1="60.96" y1="-63.5" x2="66.04" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-81.28" x2="60.96" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-81.28" x2="60.96" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="D22" gate="1" pin="A"/>
<wire x1="60.96" y1="-88.9" x2="66.04" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-106.68" x2="60.96" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-106.68" x2="60.96" y2="-114.3" width="0.1524" layer="91"/>
<pinref part="D25" gate="1" pin="A"/>
<wire x1="60.96" y1="-114.3" x2="66.04" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-132.08" x2="60.96" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-132.08" x2="60.96" y2="-139.7" width="0.1524" layer="91"/>
<pinref part="D28" gate="1" pin="A"/>
<wire x1="60.96" y1="-139.7" x2="66.04" y2="-139.7" width="0.1524" layer="91"/>
<wire x1="55.88" y1="96.52" x2="55.88" y2="71.12" width="0.1524" layer="91"/>
<wire x1="55.88" y1="71.12" x2="55.88" y2="45.72" width="0.1524" layer="91"/>
<wire x1="55.88" y1="45.72" x2="55.88" y2="20.32" width="0.1524" layer="91"/>
<wire x1="55.88" y1="20.32" x2="55.88" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-5.08" x2="55.88" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-30.48" x2="55.88" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-55.88" x2="55.88" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-81.28" x2="55.88" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-106.68" x2="55.88" y2="-132.08" width="0.1524" layer="91"/>
<junction x="55.88" y="-106.68"/>
<junction x="55.88" y="-81.28"/>
<junction x="55.88" y="-55.88"/>
<junction x="55.88" y="-30.48"/>
<junction x="55.88" y="-5.08"/>
<junction x="55.88" y="20.32"/>
<junction x="55.88" y="45.72"/>
<junction x="55.88" y="71.12"/>
<wire x1="55.88" y1="-162.56" x2="55.88" y2="-132.08" width="0.1524" layer="91"/>
<junction x="55.88" y="-132.08"/>
<pinref part="X2" gate="-1" pin="KL"/>
<pinref part="X19" gate="-2" pin="KL"/>
<wire x1="66.04" y1="-132.08" x2="60.96" y2="-132.08" width="0.1524" layer="91"/>
<pinref part="X18" gate="-1" pin="KL"/>
<junction x="60.96" y="-132.08"/>
<wire x1="66.04" y1="-106.68" x2="60.96" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="X16" gate="-1" pin="KL"/>
<junction x="60.96" y="-106.68"/>
<wire x1="66.04" y1="-81.28" x2="60.96" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="X14" gate="-1" pin="KL"/>
<junction x="60.96" y="-81.28"/>
<wire x1="66.04" y1="-55.88" x2="60.96" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="X12" gate="-1" pin="KL"/>
<junction x="60.96" y="-55.88"/>
<wire x1="66.04" y1="-5.08" x2="60.96" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="X10" gate="-1" pin="KL"/>
<junction x="60.96" y="-5.08"/>
<wire x1="66.04" y1="20.32" x2="60.96" y2="20.32" width="0.1524" layer="91"/>
<pinref part="X8" gate="-1" pin="KL"/>
<junction x="60.96" y="20.32"/>
<wire x1="66.04" y1="45.72" x2="60.96" y2="45.72" width="0.1524" layer="91"/>
<pinref part="X6" gate="-1" pin="KL"/>
<junction x="60.96" y="45.72"/>
<wire x1="66.04" y1="71.12" x2="60.96" y2="71.12" width="0.1524" layer="91"/>
<pinref part="X4" gate="-1" pin="KL"/>
<junction x="60.96" y="71.12"/>
<wire x1="66.04" y1="-30.48" x2="60.96" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="X28" gate="-1" pin="KL"/>
<junction x="60.96" y="-30.48"/>
<junction x="78.74" y="-162.56"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="104.14" y1="-162.56" x2="81.28" y2="-162.56" width="0.1524" layer="91"/>
<pinref part="D3" gate="1" pin="C"/>
<wire x1="101.6" y1="96.52" x2="104.14" y2="96.52" width="0.1524" layer="91"/>
<wire x1="104.14" y1="96.52" x2="104.14" y2="71.12" width="0.1524" layer="91"/>
<wire x1="104.14" y1="71.12" x2="104.14" y2="45.72" width="0.1524" layer="91"/>
<wire x1="104.14" y1="45.72" x2="104.14" y2="20.32" width="0.1524" layer="91"/>
<wire x1="104.14" y1="20.32" x2="104.14" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-5.08" x2="104.14" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-30.48" x2="104.14" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-55.88" x2="104.14" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-81.28" x2="104.14" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-106.68" x2="104.14" y2="-132.08" width="0.1524" layer="91"/>
<pinref part="D6" gate="1" pin="C"/>
<wire x1="101.6" y1="71.12" x2="104.14" y2="71.12" width="0.1524" layer="91"/>
<junction x="104.14" y="71.12"/>
<pinref part="D9" gate="1" pin="C"/>
<wire x1="101.6" y1="45.72" x2="104.14" y2="45.72" width="0.1524" layer="91"/>
<junction x="104.14" y="45.72"/>
<pinref part="D12" gate="1" pin="C"/>
<wire x1="101.6" y1="20.32" x2="104.14" y2="20.32" width="0.1524" layer="91"/>
<junction x="104.14" y="20.32"/>
<pinref part="D15" gate="1" pin="C"/>
<wire x1="101.6" y1="-5.08" x2="104.14" y2="-5.08" width="0.1524" layer="91"/>
<junction x="104.14" y="-5.08"/>
<pinref part="D21" gate="1" pin="C"/>
<wire x1="101.6" y1="-55.88" x2="104.14" y2="-55.88" width="0.1524" layer="91"/>
<junction x="104.14" y="-55.88"/>
<pinref part="D24" gate="1" pin="C"/>
<wire x1="101.6" y1="-81.28" x2="104.14" y2="-81.28" width="0.1524" layer="91"/>
<junction x="104.14" y="-81.28"/>
<pinref part="D30" gate="1" pin="C"/>
<wire x1="101.6" y1="-132.08" x2="104.14" y2="-132.08" width="0.1524" layer="91"/>
<pinref part="D27" gate="1" pin="C"/>
<wire x1="101.6" y1="-106.68" x2="104.14" y2="-106.68" width="0.1524" layer="91"/>
<junction x="104.14" y="-106.68"/>
<pinref part="D18" gate="1" pin="C"/>
<wire x1="101.6" y1="-30.48" x2="104.14" y2="-30.48" width="0.1524" layer="91"/>
<junction x="104.14" y="-30.48"/>
<wire x1="104.14" y1="-162.56" x2="104.14" y2="-132.08" width="0.1524" layer="91"/>
<junction x="104.14" y="-132.08"/>
<pinref part="X19" gate="-1" pin="KL"/>
<junction x="81.28" y="-162.56"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="144.78" y1="-81.28" x2="152.4" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="D32" gate="1" pin="A"/>
<pinref part="X20" gate="-1" pin="KL"/>
<junction x="144.78" y="-81.28"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="144.78" y1="-93.98" x2="152.4" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="D31" gate="1" pin="A"/>
<pinref part="X21" gate="-1" pin="KL"/>
<junction x="144.78" y="-93.98"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="144.78" y1="-106.68" x2="152.4" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="D33" gate="1" pin="A"/>
<pinref part="X22" gate="-1" pin="KL"/>
<junction x="144.78" y="-106.68"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="144.78" y1="-119.38" x2="152.4" y2="-119.38" width="0.1524" layer="91"/>
<pinref part="D34" gate="1" pin="A"/>
<pinref part="X23" gate="-1" pin="KL"/>
<junction x="144.78" y="-119.38"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<wire x1="144.78" y1="-132.08" x2="152.4" y2="-132.08" width="0.1524" layer="91"/>
<pinref part="D35" gate="1" pin="A"/>
<pinref part="X24" gate="-1" pin="KL"/>
<junction x="144.78" y="-132.08"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<wire x1="144.78" y1="-144.78" x2="152.4" y2="-144.78" width="0.1524" layer="91"/>
<pinref part="D36" gate="1" pin="A"/>
<pinref part="X25" gate="-1" pin="KL"/>
<junction x="144.78" y="-144.78"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="142.24" y1="-81.28" x2="132.08" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-81.28" x2="132.08" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-93.98" x2="132.08" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-106.68" x2="132.08" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-119.38" x2="132.08" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-132.08" x2="132.08" y2="-144.78" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-144.78" x2="132.08" y2="-165.1" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-165.1" x2="142.24" y2="-165.1" width="0.1524" layer="91"/>
<wire x1="142.24" y1="-93.98" x2="132.08" y2="-93.98" width="0.1524" layer="91"/>
<junction x="132.08" y="-93.98"/>
<wire x1="142.24" y1="-106.68" x2="132.08" y2="-106.68" width="0.1524" layer="91"/>
<junction x="132.08" y="-106.68"/>
<wire x1="142.24" y1="-119.38" x2="132.08" y2="-119.38" width="0.1524" layer="91"/>
<junction x="132.08" y="-119.38"/>
<wire x1="142.24" y1="-132.08" x2="132.08" y2="-132.08" width="0.1524" layer="91"/>
<junction x="132.08" y="-132.08"/>
<wire x1="142.24" y1="-144.78" x2="132.08" y2="-144.78" width="0.1524" layer="91"/>
<junction x="132.08" y="-144.78"/>
<pinref part="X20" gate="-2" pin="KL"/>
<pinref part="X21" gate="-2" pin="KL"/>
<junction x="142.24" y="-93.98"/>
<junction x="142.24" y="-81.28"/>
<pinref part="X22" gate="-2" pin="KL"/>
<junction x="142.24" y="-106.68"/>
<pinref part="X23" gate="-2" pin="KL"/>
<junction x="142.24" y="-119.38"/>
<pinref part="X24" gate="-2" pin="KL"/>
<junction x="142.24" y="-132.08"/>
<pinref part="X25" gate="-2" pin="KL"/>
<junction x="142.24" y="-144.78"/>
<pinref part="X26" gate="-2" pin="KL"/>
<junction x="142.24" y="-165.1"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="D32" gate="1" pin="C"/>
<wire x1="157.48" y1="-81.28" x2="160.02" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-81.28" x2="160.02" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-93.98" x2="160.02" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-106.68" x2="160.02" y2="-119.38" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-119.38" x2="160.02" y2="-132.08" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-132.08" x2="160.02" y2="-144.78" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-144.78" x2="160.02" y2="-165.1" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-165.1" x2="144.78" y2="-165.1" width="0.1524" layer="91"/>
<pinref part="D31" gate="1" pin="C"/>
<wire x1="160.02" y1="-93.98" x2="157.48" y2="-93.98" width="0.1524" layer="91"/>
<junction x="160.02" y="-93.98"/>
<pinref part="D33" gate="1" pin="C"/>
<wire x1="160.02" y1="-106.68" x2="157.48" y2="-106.68" width="0.1524" layer="91"/>
<junction x="160.02" y="-106.68"/>
<pinref part="D34" gate="1" pin="C"/>
<wire x1="160.02" y1="-119.38" x2="157.48" y2="-119.38" width="0.1524" layer="91"/>
<junction x="160.02" y="-119.38"/>
<pinref part="D35" gate="1" pin="C"/>
<wire x1="160.02" y1="-132.08" x2="157.48" y2="-132.08" width="0.1524" layer="91"/>
<junction x="160.02" y="-132.08"/>
<pinref part="D36" gate="1" pin="C"/>
<wire x1="160.02" y1="-144.78" x2="157.48" y2="-144.78" width="0.1524" layer="91"/>
<junction x="160.02" y="-144.78"/>
<pinref part="X26" gate="-1" pin="KL"/>
<junction x="144.78" y="-165.1"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="D17" gate="1" pin="A"/>
<pinref part="D16" gate="1" pin="C"/>
<wire x1="71.12" y1="-38.1" x2="76.2" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-38.1" x2="81.28" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-30.48" x2="76.2" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-30.48" x2="81.28" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-30.48" x2="76.2" y2="-38.1" width="0.1524" layer="91"/>
<junction x="76.2" y="-30.48"/>
<pinref part="X28" gate="-2" pin="KL"/>
<pinref part="X27" gate="-1" pin="KL"/>
<junction x="76.2" y="-38.1"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
